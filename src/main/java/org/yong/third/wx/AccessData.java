package org.yong.third.wx;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class AccessData {

    private static final String URL_OF_USER_IFO     = "https://api.weixin.qq.com/sns/userinfo" + "?access_token={ACCESS_TOKEN}" + "&openid={OPEN_ID}" + "&lang=zh_CN";

    private static final String KEY_OF_ACCESS_TOKEN = "{ACCESS_TOKEN}";

    private static final String KEY_OF_OPEN_ID      = "{OPEN_ID}";

    private String              accessToken;

    private String              expiresIn;

    private String              refreshToken;

    private String              openId;

    private String              scope;

    @SuppressWarnings("rawtypes")
    static AccessData init(Map param) {
        AccessData accessData = new AccessData();
        accessData.accessToken = String.valueOf(param.get("access_token"));
        accessData.expiresIn = String.valueOf(param.get("expires_in"));
        accessData.refreshToken = String.valueOf(param.get("refresh_token"));
        accessData.openId = String.valueOf(param.get("openid"));
        accessData.scope = String.valueOf(param.get("scope"));
        return accessData;
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    public WeChatUser getUser() {
        String url = URL_OF_USER_IFO.replace(KEY_OF_ACCESS_TOKEN, this.accessToken).replace(KEY_OF_OPEN_ID, this.openId);

        String data = WeChatUtil.parseHttpGetContent(url);
        boolean failed = StringUtils.isBlank(data);
        if (failed)
            return null;

        JSONObject jsonObject = JSON.parseObject(data);
        return WeChatUser.init(jsonObject);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getOpenId() {
        return openId;
    }

    public String getScope() {
        return scope;
    }

}
