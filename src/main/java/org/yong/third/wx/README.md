# 微信公众号快速接入
3步轻松搞定微信公众号授权问题, [官方文档](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842)  
* 创建授权重定向URL  
  ```java
  public static String createUrl(AuthorizeParam param)
  ```  
  
* 获取AccessData/AccessToken  
  ```java
  public static AccessData accessToken(AccessParam param)
  ```  

* 通过AccessData获取WeChatUser  
  ```java
  public WeChatUser getUser();
  ```
  
# 参数构建
  ```java
  // 授权重定向页面参数
  public static AuthorizeParam newAuthorizeParam(String appId, String redirectUrl, Scope scope, String callbackParam)

  // 用户信息查询参数
  public static AccessParam newAccessParam(String code, String appId, String appSecurity)
  ```
  
# Maven依赖
```
<dependency>
  <groupId>com.alibaba</groupId>
  <artifactId>fastjson</artifactId>
  <version>1.1.41</version>
</dependency>
<dependency>
  <groupId>org.apache.httpcomponents</groupId>
  <artifactId>httpclient</artifactId>
  <version>4.5.3</version>
</dependency>
<dependency>
  <groupId>org.apache.commons</groupId>
  <artifactId>commons-lang3</artifactId>
  <version>3.6</version>
</dependency>
```