package org.yong.third.wx;

import java.util.Map;

public class WeChatUser {
    private String openId;

    private String nickname;

    private String sex;

    private String province;

    private String city;

    private String country;

    private String headImgUrl;

    private String privileges;

    private String unionid;

    @SuppressWarnings("rawtypes")
    static WeChatUser init(Map data) {
        WeChatUser user = new WeChatUser();
        user.openId = String.valueOf(data.get("openid"));
        user.nickname = String.valueOf(data.get("nickname"));
        user.sex = String.valueOf(data.get("sex"));
        user.province = String.valueOf(data.get("province"));
        user.city = String.valueOf(data.get("city"));
        user.country = String.valueOf(data.get("country"));
        user.headImgUrl = String.valueOf(data.get("headimgurl"));
        user.unionid = String.valueOf(data.get("unionid"));
        user.privileges = String.valueOf(data.get("privilege"));
        return user;
    }

    public String getOpenId() {
        return openId;
    }

    public String getNickname() {
        return nickname;
    }

    public String getSex() {
        return sex;
    }

    public String getProvince() {
        return province;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public String getPrivileges() {
        return privileges;
    }

    public String getUnionid() {
        return unionid;
    }
}