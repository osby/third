package org.yong.third.wx;

import java.io.Closeable;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 使用步骤: <br>
 * 1. 获取授权URL: {@link WeChatUtil#createUrl(AuthorizeParam)} <br>
 * 2. 获取Token: {@link WeChatUtil#accessToken(AccessParam)} <br>
 * 3. 获取用户信息 {@link AccessData#getUser()}
 */
public class WeChatUtil {

    /**
     * 授权请求
     */
    private static final String AUTH_URL      = "https://open.weixin.qq.com/connect/oauth2/authorize" + "?appid={APP_ID}" + "&redirect_uri={REDIRECT_URI}" + "&response_type=code"
                                                      + "&scope={SCOPE}" + "&state={STATE}" + "#wechat_redirect";

    /**
     * 获取Token
     */
    private static final String GET_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token" + "?appid={APP_ID}" + "&secret={APP_SECURITY}" + "&code={CODE}"
                                                      + "&grant_type=authorization_code";

    /**
     * 授权类型
     */
    public enum Scope {
        /**
         * 获取用户基本信息, 包括昵称头像等, 需要用户主动确认授权
         */
        SNSAPI_USERINFO("snsapi_userinfo"),
        /**
         * 仅获取openID静默授权
         */
        SNSAPI_BASE("snsapi_base");
        private final String VALUE;

        private Scope(String val) {
            this.VALUE = val;
        }
    }

    /**
     * 授权页面URL参数构建
     *
     * @param appId
     *            公众号AppId
     * @param redirectUrl
     *            微信回调处理接口
     * @param scope
     *            用户信息获取类型
     * @param callbackParam
     *            回调接口自定义参数
     * @return URL参数
     */
    public static AuthorizeParam newAuthorizeParam(String appId, String redirectUrl, Scope scope, String callbackParam) {
        return new AuthorizeParam(appId, redirectUrl, scope, StringUtils.trimToEmpty(callbackParam));
    }

    /**
     * 创建授权参数
     *
     * @param code
     *            回调Code
     * @param appId
     *            公众号APPId
     * @param appSecurity
     *            公众号 Security
     * @return 授权参数
     */
    public static AccessParam newAccessParam(String code, String appId, String appSecurity) {
        return new AccessParam(appId, appSecurity, code);
    }

    /**
     * 创建授权URL
     *
     * @param param
     *            授权参数
     * @return 授权URL地址
     * @see #newAuthorizeParam(String, String, Scope, String)
     */
    public static String createUrl(AuthorizeParam param) {
        return AUTH_URL.replace(AuthorizeParam.KEY_OF_APP_ID, param.appId).replace(AuthorizeParam.KEY_OF_REDIRECT_URL, param.redirectUrl)
                .replace(AuthorizeParam.KEY_OF_STATE, param.state).replace(AuthorizeParam.KEY_OF_SCOPE, param.scope.VALUE);
    }

    /**
     * 获取授权数据
     *
     * @param param
     *            授权参数
     * @return 授权数据
     * @see #newAccessParam(String, String, String)
     */
    public static AccessData accessToken(AccessParam param) {
        String url = GET_TOKEN_URL.replace(AccessParam.KEY_OF_APP_ID, param.appId).replace(AccessParam.KEY_OF_CODE, param.code)
                .replace(AccessParam.KEY_OF_SECURITY, param.appSecurity);
        return sendUrlAndGetAccessData(url);
    }

    /**
     * 使用Get方式请求url
     *
     * @param url
     *            目标地址
     * @return 响应内容, 失败返回null
     */
    public static String parseHttpGetContent(String url) {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet get = new HttpGet(url);
        CloseableHttpResponse resp = null;
        try {
            resp = client.execute(get);
            int code = resp.getStatusLine().getStatusCode();
            if (200 == code) {
                HttpEntity entity = resp.getEntity();
                String data = EntityUtils.toString(entity);
                data = new String(data.getBytes("ISO8859-1"), "utf-8");
                return data;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeQuietly(client);
            closeQuietly(resp);
        }
        return null;
    }

    private static void closeQuietly(Closeable c) {
        if (null != c)
            try {
                c.close();
            } catch (Exception e) {
            }
    }

    private static AccessData sendUrlAndGetAccessData(String url) {
        String data = parseHttpGetContent(url);
        boolean failed = StringUtils.isBlank(data);
        if (failed)
            return null;

        JSONObject jsonObject = JSON.parseObject(data);
        return AccessData.init(jsonObject);
    }

    static class AuthorizeParam {
        String              appId;

        static final String KEY_OF_APP_ID       = "{APP_ID}";

        String              redirectUrl;

        static final String KEY_OF_REDIRECT_URL = "{REDIRECT_URI}";

        Scope               scope;

        static final String KEY_OF_SCOPE        = "{SCOPE}";

        String              state;

        static final String KEY_OF_STATE        = "{STATE}";

        public AuthorizeParam(String appId, String redirectUrl, Scope scope, String state) {
            this.appId = appId;
            this.redirectUrl = redirectUrl;
            this.scope = scope;
            this.state = state;
        }
    }

    static class AccessParam {
        String              appId;

        static final String KEY_OF_APP_ID   = "{APP_ID}";

        String              appSecurity;

        static final String KEY_OF_SECURITY = "{APP_SECURITY}";

        String              code;

        static final String KEY_OF_CODE     = "{CODE}";

        public AccessParam(String appId, String appSecurity, String code) {
            this.appId = appId;
            this.appSecurity = appSecurity;
            this.code = code;
        }
    }
}
